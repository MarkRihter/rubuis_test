import React, { FC } from 'react'

import UsersPage from './pages/UsersPage'

const App: FC = () => <UsersPage />


export default App
