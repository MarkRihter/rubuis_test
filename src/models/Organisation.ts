export default interface Organisation {
  id: number;
  fullName: string;
  shortName: string;
}
