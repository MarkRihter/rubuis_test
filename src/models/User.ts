export default interface User {
	id: number;
	firstName: string;
	lastName: string;
	middleName: string;
	organisationId: number;
	email: string;
}

export type FormUser = Omit<User, 'id' | 'organisationId'> & { organisationId: string; }

export function isUser(obj: any): obj is User {
	return typeof obj.id === 'number' &&
		typeof obj.firstName === 'string' &&
		typeof obj.lastName === 'string' &&
		typeof obj.middleName === 'string' &&
		typeof obj.organisationId === 'number' &&
		typeof obj.email === 'string'
}

export type FormUserKeys = keyof FormUser
export function isOneOfFormUserKeys(key: string): key is FormUserKeys {
	return ['firstName', 'lastName', 'middleName', 'organisationId', 'email'].includes(key)
}