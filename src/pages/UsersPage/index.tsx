import React, { FC, useEffect, useState } from 'react'

import { Container, IconButton, Stack, Card, CardHeader, Button } from '@mui/material'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/Delete'
import AddIcon from '@mui/icons-material/Add'
import { DataGrid, GridColDef, GridValueGetterParams, GridRenderCellParams } from '@mui/x-data-grid'
import { useAppSelector, useAppDispatch } from '../../store/hooks'
import { getInitialData, removeUsers, updateUser } from './slice'
import User, { isUser } from '../../models/User'
import DeleteDialog from './components/DeleteDialog'
import UserInfo from './components/UserInfo'

import './index.scss'

const UsersPage: FC = () => {

	const [usersOnPage, setUsersOnPage] = useState(10)
	const [selectedRows, setSelectedRows] = useState<number[]>([])
	const [usersToDelete, setUsersToDelete] = useState<number[] | undefined>()

	const [isUserInfoOpened, setUserInfoOpened] = useState(false)
	const [selectedUserToEdit, setSelectedUserToEdit] = useState<User | undefined>()

	const users = useAppSelector(state => state.users)
	const organisations = useAppSelector(state => state.organisations)
	const dispatch = useAppDispatch()

	useEffect(() => {
		dispatch(getInitialData())
	}, [])

	useEffect(() => {
		if (!isUserInfoOpened)
			setSelectedUserToEdit(undefined)
	}, [isUserInfoOpened])

	const columns: GridColDef[] = [
		{ 
			field: 'id', 
			headerName: 'ID', 
			width: 90,
		},
		{
			field: 'lastName',
			flex: 1,
			headerName: 'Фамилия',
		},
		{
			field: 'firstName',
			flex: 1,
			headerName: 'Имя',
		},
		{
			field: 'middleName',
			flex: 1,
			headerName: 'Отчество',
		},
		{
			field: 'organisationId',
			flex: 1,
			headerName: 'Организация',
			valueGetter: (params: GridValueGetterParams) => `${organisations.find(({id}) => id === params.value)?.shortName ?? ''}`
		},
		{
			field: 'email',
			flex: 1,
			headerName: 'Email',
		},
		{
			field: 'Действия',
			flex: 1,
			resizable: false,
			renderCell: (params: GridRenderCellParams) =>  <Stack direction="row" spacing={1}>
				<IconButton onClick={event => {
					event.stopPropagation()
					setUsersToDelete([params.getValue(params.id, 'id') as number])
				}}>
					<DeleteIcon />
				</IconButton>
				<IconButton onClick={event => {
					event.stopPropagation()
					setUserInfoOpened(true)
					if(isUser(params.row)) setSelectedUserToEdit(params.row)
				}}>
					<EditIcon />
				</IconButton>
			</Stack>
		},
	]

	return <Container className="user-page">
		<Card variant="outlined">
			<CardHeader
				action={<Stack direction="row" spacing={1}>{selectedRows.length > 0 &&
					<Button color="error" variant="outlined" startIcon={<DeleteIcon />} onClick={() => setUsersToDelete(selectedRows)}>
						Удалить
					</Button>}
				<Button variant="outlined" startIcon={<AddIcon />} onClick={() => setUserInfoOpened(true)}>
					Добавить пользователя
				</Button>
				</Stack>
				}
				title="Пользователи"
			/>
			<DataGrid
				disableColumnFilter
				disableColumnMenu
				autoHeight
				selectionModel={selectedRows}
				onSelectionModelChange={model => setSelectedRows(model as number[])}
				onPageSizeChange={setUsersOnPage}
				rows={users}
				columns={columns}
				pageSize={usersOnPage}
				rowsPerPageOptions={[5, 10, 25]}
				checkboxSelection
			/>
		</Card>

		<DeleteDialog 
			isOpen={!!usersToDelete}
			isForMultipleRecords={(usersToDelete?.length ?? 0) > 1}
			onClose={() => setUsersToDelete(undefined)}
			onDelete={() => dispatch(removeUsers(usersToDelete ?? []))}
		/>

		<UserInfo 
			isOpen={isUserInfoOpened}
			onClose={() => setUserInfoOpened(false)}
			onUpdate={updatedUser => dispatch(updateUser(updatedUser))}
			user={selectedUserToEdit}
		/>
	</Container> 
}

export default UsersPage
