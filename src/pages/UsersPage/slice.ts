import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import User, { FormUser } from '../../models/User'
import Organisation from '../../models/Organisation'

import usersJson from '../../data/users.json'
import organisationsJson from '../../data/organisations.json'

interface UsersState {
	users: User[];
	organisations: Organisation[],
}

const initialState: UsersState = {
	users: [],
	organisations: [],
}

export const usersSlice = createSlice({
	name: 'main',
	initialState,
	reducers: {
		getInitialData(state) {
			state.users = usersJson
			state.organisations = organisationsJson
		},
		addUser(state, action: PayloadAction<User>) {
			state.users.push(action.payload)
		},
		removeUsers(state, action: PayloadAction<number[]>) {
			state.users = state.users.filter(({ id }) => !action.payload.includes(id))
		},
		updateUser(state, action: PayloadAction<{ formUser: FormUser, id?: number }>) {
			const { formUser, id } = action.payload
			const { organisationId, ...restOfFormUser } = formUser
			const userToUpdate = state.users.find(({ id: existingUserId }) => id === existingUserId)
			if (userToUpdate && id)
				state.users[state.users.indexOf(userToUpdate)] = { ...restOfFormUser, organisationId: +organisationId, id }
			else
				state.users.push({ ...restOfFormUser, organisationId: +organisationId, id: Math.max(...state.users.map(({ id }) => id)) + 1 })
		}
	},
})

export const { getInitialData, addUser, removeUsers, updateUser } = usersSlice.actions

export default usersSlice.reducer
