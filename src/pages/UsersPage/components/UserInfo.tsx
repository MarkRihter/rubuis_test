import React, { FC, useState, ChangeEvent, useEffect } from 'react'
import { useAppSelector } from '../../../store/hooks'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField, MenuItem, InputLabel, FormControl } from '@mui/material'
import Select, { SelectChangeEvent } from '@mui/material/Select'
import User, { FormUser, isOneOfFormUserKeys } from '../../../models/User'


interface UserInfoProps {
	isOpen: boolean;
	user?: User;
	onClose: () => void;
	onUpdate: (payload: { formUser: FormUser, id?: number }) => void;
}

const DEFAULT_FORM_STATE: FormUser = {
	firstName: '',
	lastName: '',
	middleName: '',
	organisationId: '',
	email: '',
}

const UserInfo: FC<UserInfoProps> = ({isOpen, user, onClose, onUpdate}) => {

	const [formUser, setFormUser] = useState(DEFAULT_FORM_STATE)

	const organisations = useAppSelector(state => state.organisations)

	useEffect(() => {
		if(!isOpen) setFormUser(DEFAULT_FORM_STATE)
	}, [isOpen])

	useEffect(() => {
		if (user) {
			const { id, organisationId, ...restOfUser } = user
			setFormUser({...restOfUser, organisationId: `${organisationId}`})
		}
	}, [user])

	const onChange = (event: ChangeEvent) => {
		const { id } = event.target
		if (isOneOfFormUserKeys(id)) {
			const newFormUser: FormUser = {...formUser}
			const { value } =  (event.target as HTMLInputElement)
			newFormUser[id] = value
			setFormUser(newFormUser)
		}
	}

	const onSelect = (event: SelectChangeEvent) => {
		setFormUser({...formUser, organisationId: event.target.value})
	}

	return <Dialog
		fullWidth
		open={isOpen}
		onClose={onClose}
	>
		<form onSubmit={e => {
			e.preventDefault()
			onUpdate({formUser, id: user?.id})
			onClose()
		}}>
			<DialogTitle id="alert-dialog-title">
				{ user ? 'Редактировать' : 'Создать' } пользователя
			</DialogTitle>
			<DialogContent className='form-content'>
				<TextField fullWidth value={formUser.lastName} onChange={onChange}  required id="lastName" label="Фамилия" variant="outlined" />
				<TextField fullWidth value={formUser.firstName} onChange={onChange} required id="firstName" label="Имя" variant="outlined" />
				<TextField fullWidth value={formUser.middleName} onChange={onChange} id="middleName" label="Отчество" variant="outlined" />
				<FormControl fullWidth>
					<InputLabel id="organisation-id">Организация</InputLabel>
					<Select
						required
						labelId="organisation-id"
						id="demo-simple-select"
						value={formUser.organisationId}
						label="Организация"
						onChange={onSelect}
					>
						{organisations.map(({id, fullName}) => <MenuItem key={id} value={id}>{fullName}</MenuItem>)}
					</Select>
				</FormControl>
				<TextField fullWidth value={formUser.email} onChange={onChange} required type="email" id="email" label="Email" variant="outlined" />
			</DialogContent>
			<DialogActions>
				<Button type="button" color="secondary" onClick={onClose}>Отмена</Button>
				<Button type="submit">
					Сохранить
				</Button>
			</DialogActions>
		</form>
	</Dialog>
}

export default UserInfo