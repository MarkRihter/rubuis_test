import React, { FC } from 'react'
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material'

interface DeleteDialogProps {
	isOpen: boolean;
	isForMultipleRecords: boolean;
	onClose: () => void;
	onDelete: () => void;
}

const DeleteDialog: FC<DeleteDialogProps> = ({isOpen, isForMultipleRecords, onClose, onDelete}) => {
	return 	<Dialog
		open={isOpen}
		onClose={onClose}
		aria-labelledby="alert-dialog-title"
		aria-describedby="alert-dialog-description"
	>
		<DialogTitle id="alert-dialog-title">
      Удалить {`${isForMultipleRecords ? 'пользователей' : 'пользователя'}`}
		</DialogTitle>
		<DialogContent>
			<DialogContentText>
				{`${isForMultipleRecords ? 'Выбранные записи будут удалены' : 'Выбранная запись будет удалена'}`}. Вы уверены?
			</DialogContentText>
		</DialogContent>
		<DialogActions>
			<Button onClick={onClose}>Отмена</Button>
			<Button color="error" onClick={() => {
				onDelete()
				onClose()
			}}
			>
				Да
			</Button>
		</DialogActions>
	</Dialog>
}

export default DeleteDialog